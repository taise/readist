json.extract! @book, :id, :asin, :title, :isbn, :author, :product_group, :manufacturer, :publication_date, :small_image, :medium_image, :large_image, :created_at, :updated_at

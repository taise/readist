json.array!(@books) do |book|
  json.extract! book, :id, :asin, :title, :isbn, :author, :product_group, :manufacturer, :publication_date, :small_image, :medium_image, :large_image
  json.url book_url(book, format: :json)
end

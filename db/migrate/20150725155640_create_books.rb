class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.bigint :asin
      t.string :title
      t.bigint :isbn
      t.string :author
      t.string :product_group
      t.string :manufacturer
      t.date :publication_date
      t.string :small_image
      t.string :medium_image
      t.string :large_image

      t.timestamps null: false
    end
  end
end
